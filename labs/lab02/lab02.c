#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.
/**
 * @brief floatCal approximates the value of Pi using float data type
 * 
 * @param iteration - inputs the number of iteration with a default value of 100000
 * @return float - closest approximation of Pi
 */
float floatCal(int iteration){
  int i = 2;
  float result = 1.0;
  while(iteration--){
    result = result*i*i/(i-1)/(i+1);  //Wallis product
    i=i+2;
  }
  return result*2;
}
/**
 * @brief doubleCal approximates the value of Pi using double data type
 * 
 * @param iteration  - inputs the number of iterations with a default value of 100000
 * @return double - closest approximation of Pi
 */
double doubleCal(int iteration){
  int i = 2;
  double result = 1.0;
  while(iteration--){
    result = result*i*i/(i-1)/(i+1); //Wallis product
    i=i+2;
  }
  return result*2;
}
/**
 * @brief LAB #02 - TEMPLATE
 *        Main entry point for the code.
 * 
 * @return int      Returns exit-status zero on completion.
 */
int main() {
    stdio_init_all();
    int iteration = 100000; //recommended value
    float compareTo = 3.14159265359; //recommended value
    float floatResult = floatCal(iteration);
    double doubleResult = doubleCal(iteration);
    float floatErr = abs(floatResult - compareTo) * 100 / compareTo;  //error calculation in %
    double doubleErr = abs(doubleResult - compareTo) * 100 / compareTo; //error calculation in %
    printf("Result in float is %f and error is %f%%\n",floatResult, floatErr);
    printf("Result in double is %f and error is %f%%\n",doubleResult, doubleErr);
    return 0;
}
