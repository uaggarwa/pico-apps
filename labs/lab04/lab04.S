.syntax unified                 @ Specify unified assembly syntax
.cpu    cortex-m0plus           @ Specify CPU type is Cortex M0+
.thumb                          @ Specify thumb assembly for RP2040
.global main_asm                @ Provide program starting address to the linker
.align 4                        @ Specify code alignment

.equ    SLEEP_TIME, 500         @ Specify the amount of ms that we want to sleep for in the loop
.equ    LED_GPIO_PIN, 25        @ Specifiy the physical GPIO pin that the LED is connected to
.equ    LED_GPIO_OUT, 1         @ Specify the direction that we want to set the GPIO pin to
.equ    LED_VALUE_ON, 1         @ Specify the value that turns the LED "on"
.equ    LED_VALUE_OFF, 0        @ Specify the value that turns the LED "off"


@ Entry point to the ASM portion of the program
main_asm:
    MOVS    R0, #LED_GPIO_PIN           @ GPIO LED pin on PI PICO board
    BL      asm_gpio_init               @ Call subroutine to initialise GPIO pin specified by R0
    MOVS    R0, #LED_GPIO_PIN           @ GPIO LED pin on PI PICO board
    MOVS    R1, #LED_GPIO_OUT           @ GPIO pin to be setup as an output pin
    BL      asm_gpio_set_dir            @ Call subroutine to set GPIO pin specified by R0 to state specified by R1
loop:
    LDR     R0, =SLEEP_TIME             @ Set value of SLEEP_TIME
    BL      sleep_ms                    @ Sleep until SLEEP_TIME has elapsed then toggle LED GPIO pin
    BL      sub_toggle                  @ Call subroutine to toggle current LED GPIO pin value
    B       loop                        @ Repeat loop

@ Subroutine to toggle the LED GPIO pin value
sub_toggle:
    push    {lr}                        @ Store link register to stack nested subroutines
    MOVS    R0, #LED_GPIO_PIN           @ Set LED GPIO pin number to R0 for use by asm_gpio_get
    BL      asm_gpio_get                @ Get current value of LED GPIO pin (returns to R0)
    CMP     R0, #LED_VALUE_OFF          @ Check if LED GPIO pin value is "off"
    BEQ     led_set_on                  @ If "off" then branch code to turn it on
led_set_off:
    MOVS    R1, #LED_VALUE_OFF          @ LED is currently "on" thus turn it "off"
    b       led_set_state               @ Branch to portion of code where we set the state of LED
led_set_on:
    MOVS    R1, #LED_VALUE_ON           @ LED is currently "off" thus turn it "on"
led_set_state:
    MOVS    R0, #LED_GPIO_PIN           @ Set LED GPIO pin number to R0 for use by asm_gpio_put
    BL      asm_gpio_put                @ Update value of LED GPIO pin (based on value in R1)
    POP     {pc}                        @ Pop link register from stack to program counter


@ Set data alignment
.data
    .align 4
